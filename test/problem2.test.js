
const problem2 = require('../problem2.cjs');
const inventory = require('../inventory.cjs');


test('Problem2', () => {
    expect(problem2(inventory)).toStrictEqual(
        { "id": 50, "car_make": "Lincoln", "car_model": "Town Car", "car_year": 1999 }
    );
});