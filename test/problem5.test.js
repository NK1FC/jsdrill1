
const inventory = require('../inventory.cjs');

const problem4 = require('../problem4.cjs');
const problem5 = require('../problem5.cjs');

const years = problem4(inventory);

test('Problem5', ()=>{
    expect(problem5(years)).toBe(25);
});